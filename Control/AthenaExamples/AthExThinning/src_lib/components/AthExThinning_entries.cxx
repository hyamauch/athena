#include "AthExThinning/CreateData.h"
#include "AthExThinning/WriteThinnedData.h"
#include "AthExThinning/ReadThinnedData.h"
#include "AthExThinning/SlimmerAlg.h"

  
DECLARE_COMPONENT( AthExThinning::CreateData )
DECLARE_COMPONENT( AthExThinning::WriteThinnedData )
DECLARE_COMPONENT( AthExThinning::ReadThinnedData )
DECLARE_COMPONENT( AthExThinning::SlimmerAlg )

